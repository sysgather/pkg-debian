sysgather (1.0~10-4) unstable; urgency=low

  * Update the copyright file:
    - convert it to copyright-format/1.0
    - properly prefix an empty line with a dot
    - bump the copyright year for the Debian packaging
    - change the Debian packaging license to match upstream
  * Bump Standards-Version to 3.9.4 with no changes.
  * Reflect the ages-ago move to Gitorious in the Vcs-* control fields.
  * Bump the debhelper compatibility level to 9 with no changes.

 -- Peter Pentchev <roam@ringlet.net>  Fri, 30 Aug 2013 18:05:28 +0300

sysgather (1.0~10-3) unstable; urgency=low

  * Grumble.  Fix the Debian package version - "~" instead of "pre".
  * Shorten the watchfile and replace "pre" with "~" there, too.
  * Switch to quilt for patch management.
  * Update the copyright file a bit:
    - convert it to the latest version of the DEP 5 proposed format
    - point to the correct license file
    - update the copyright years for the Debian packaging
  * Bump Standards-Version to 3.8.4:
    - add the Homepage control field
  * Bump the debhelper compatibility level to 7 and minimize
    the rules file using debhelper override targets.
  * Add misc:Depends and perl:Depends to the binary package.
  * Convert to the 3.0 (quilt) format.

 -- Peter Pentchev <roam@ringlet.net>  Fri, 14 May 2010 17:21:14 +0300

sysgather (1.0~10-2) unstable; urgency=low

  * debian/compat
    - bump the debhelper compatibility level from 4 to 6
  * debian/control
    - bump the policy version to 3.7.3 (no changes)
    - change the architecture to "all" and move libconfig-inifiles-perl to
      an arch-indep depends clause
    - bump the debhelper dependency from 4 to 6
    - add the Vcs-Svn and Vcs-Browser source-stanza fields
  * debian/copyright
    - convert to the machine-parseable format
    - add in the missing copyright notices
    - note that the Debian package files are under the GPL version 2 or later
  * debian/patches/01_sysgather.conf.dpatch
    - kill the $Ringlet$ VCS tag; right now, it's more trouble than it's worth
  * debian/rules
    - do not ignore "make clean" errors
    - build an architecture-independent package

 -- Peter Pentchev <roam@ringlet.net>  Wed, 14 May 2008 11:53:42 +0300

sysgather (1.0~10-1) unstable; urgency=low

  * New upstream version.
  * Use dpatch.

 -- Peter Pentchev <roam@ringlet.net>  Thu, 10 May 2007 13:53:42 +0300

sysgather (1.0~9-1) unstable; urgency=low

  * New upstream version.
  * Remove the absolutely useless debian/dirs file.

 -- Peter Pentchev <roam@ringlet.net>  Tue, 27 Feb 2007 13:12:46 +0200

sysgather (1.0~8-1) unstable; urgency=low

  * New upstream version.
  * Update the standards version to 3.7.2; none of the changes in
    3.7.0, 3.7.1, or 3.7.2 affect the sysgather utility or package.

 -- Peter Pentchev <roam@ringlet.net>  Mon, 23 Oct 2006 15:39:05 +0300

sysgather (1.0~7-1) unstable; urgency=low

  * New upstream version.

 -- Peter Pentchev <roam@ringlet.net>  Tue, 14 Feb 2006 20:34:37 +0200

sysgather (1.0~6-1) unstable; urgency=low

  * New upstream version.

 -- Peter Pentchev <roam@ringlet.net>  Fri, 10 Feb 2006 21:57:54 +0200

sysgather (1.0~5-1) unstable; urgency=low

  * New upstream release, incorporating the Makefile and debian.conf
    fixes from 1.0~4-2.
  * This package only installs a Perl script - no need for dh_strip.

 -- Peter Pentchev <roam@ringlet.net>  Thu,  9 Feb 2006 10:59:51 +0200

sysgather (1.0~4-2) unstable; urgency=low

  * The apache package does not really contain a file named mime.types;
    it is part of the mime-support package, so  add a mime-support
    collection to deal with it.
  * Add a missing ${DESTDIR} in the Makefile's "install" target, so that
    /etc/sysgather/sysgather.conf is actually installed even if the package
    is built on a system where sysgather is already installed.  Duh.

 -- Peter Pentchev <roam@ringlet.net>  Wed,  8 Feb 2006 17:16:16 +0200

sysgather (1.0~4-1) unstable; urgency=low

  * New upstream version integrating the Makefile patch.
  * Put the full text of the two-clause BSD license in debian/copyright,
    since it is not exactly the same as the three-clause license in the
    /usr/share/common-licenses/BSD file.
  * Exclude the sample config files from being compressed.
  * Add /etc/gshadow to sysgather.conf.default's sys-auth collection.

 -- Peter Pentchev <roam@ringlet.net>  Tue,  7 Feb 2006 23:20:15 +0200

sysgather (1.0~3-1) unstable; urgency=low

  * Initial release

 -- Peter Pentchev <roam@ringlet.net>  Tue,  7 Feb 2006 12:03:43 +0200

